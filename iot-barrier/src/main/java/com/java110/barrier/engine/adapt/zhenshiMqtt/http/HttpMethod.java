package com.java110.barrier.engine.adapt.zhenshiMqtt.http;

public enum HttpMethod {
    GET, POST, PUT, DELETE, HEAD;
}
