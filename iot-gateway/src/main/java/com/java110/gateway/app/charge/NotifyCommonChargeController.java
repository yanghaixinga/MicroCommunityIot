package com.java110.gateway.app.charge;

import com.alibaba.fastjson.JSONObject;
import com.java110.bean.ResultVo;
import com.java110.core.factory.LoggerFactory;
import com.java110.dto.chargeMachine.NotifyChargeOrderDto;
import com.java110.intf.charge.INotifyChargeV1InnerServiceSMO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 通用充电桩 充电完成回调
 *
 * 主要用于测试 使用
 */
@RestController
@RequestMapping(path = "/app/charge")
public class NotifyCommonChargeController {

    private final static Logger logger = LoggerFactory.getLogger(NotifyCommonChargeController.class);

    @Autowired
    private INotifyChargeV1InnerServiceSMO notifyChargeV1InnerServiceSMOImpl;

    /**
     * <p>支付回调Api</p>
     *
     * @param request
     * @throws Exception
     */
    @RequestMapping(path = "/finish", method = RequestMethod.POST)
    public ResponseEntity<String> finishCharge(
            @RequestBody String postInfo,
            HttpServletRequest request) {

        JSONObject param = JSONObject.parseObject(postInfo);
        NotifyChargeOrderDto notifyChargeOrderDto = new NotifyChargeOrderDto();
        notifyChargeOrderDto.setMachineCode(param.getString("machineCode"));
        notifyChargeOrderDto.setPortCode(param.getString("portCode"));
        notifyChargeOrderDto.setBodyParam(postInfo);
        notifyChargeOrderDto.setReason(param.getString("reason"));

        ResultVo resultVo = notifyChargeV1InnerServiceSMOImpl.finishCharge(notifyChargeOrderDto);
        return ResultVo.createResponseEntity(resultVo);
    }

}
