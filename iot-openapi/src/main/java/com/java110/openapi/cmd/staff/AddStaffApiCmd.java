package com.java110.openapi.cmd.staff;

import com.alibaba.fastjson.JSONObject;
import com.java110.bean.dto.file.FileDto;
import com.java110.bean.dto.org.OrgDto;
import com.java110.bean.dto.org.OrgStaffRelDto;
import com.java110.bean.po.org.OrgStaffRelPo;
import com.java110.core.annotation.Java110Cmd;
import com.java110.core.cache.MappingCache;
import com.java110.core.cmd.Cmd;
import com.java110.core.cmd.CmdEvent;
import com.java110.core.constant.MappingConstant;
import com.java110.core.context.ICmdDataFlowContext;
import com.java110.core.exception.CmdException;
import com.java110.core.factory.AuthenticationFactory;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.core.utils.*;
import com.java110.dto.accessControlFace.AccessControlFaceDto;
import com.java110.dto.accessControlOrg.AccessControlOrgDto;
import com.java110.dto.storeStaff.StoreStaffDto;
import com.java110.dto.user.UserDto;
import com.java110.intf.accessControl.IAccessControlFaceV1InnerServiceSMO;
import com.java110.intf.accessControl.IAccessControlOrgV1InnerServiceSMO;
import com.java110.intf.system.IFileInnerServiceSMO;
import com.java110.intf.user.*;
import com.java110.po.accessControlFace.AccessControlFacePo;
import com.java110.po.storeStaff.StoreStaffPo;
import com.java110.po.user.UserPo;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.List;

/**
 * 接口添加员工
 * <p>
 * 员工同步只同步员工，不同步组织架构和小区授权，小区功能授权需要 在物联网系统上手工授权；
 */
@Java110Cmd(serviceCode = "staff.addStaffApi")
public class AddStaffApiCmd extends Cmd {
    private static final String STAFF_DEFAULT_PASSWORD = "123456";
    @Autowired
    private IUserV1InnerServiceSMO userV1InnerServiceSMOImpl;

    @Autowired
    private IStoreStaffV1InnerServiceSMO storeUserV1InnerServiceSMOImpl;

    @Autowired
    private IOrgV1InnerServiceSMO orgV1InnerServiceSMOImpl;

    @Autowired
    private IOrgStaffRelV1InnerServiceSMO orgStaffRelV1InnerServiceSMOImpl;


    @Autowired
    private IPrivilegeUserV1InnerServiceSMO privilegeUserV1InnerServiceSMOImpl;

    @Autowired
    private IFileInnerServiceSMO fileInnerServiceSMOImpl;

    @Autowired
    private IAccessControlOrgV1InnerServiceSMO accessControlOrgV1InnerServiceSMOImpl;

    @Autowired
    private IAccessControlFaceV1InnerServiceSMO accessControlFaceV1InnerServiceSMOImpl;

    @Autowired
    private IOrgStaffRelInnerServiceSMO orgStaffRelInnerServiceSMOImpl;


    @Override
    public void validate(CmdEvent event, ICmdDataFlowContext context, JSONObject reqJson) throws CmdException, ParseException {
        Assert.hasKeyAndValue(reqJson, "propertyId", "未包含propertyId");
        Assert.hasKeyAndValue(reqJson, "staffId", "未包含staffId");
        Assert.hasKeyAndValue(reqJson, "name", "未包含name");
        Assert.hasKeyAndValue(reqJson, "tel", "未包含link");
        Assert.hasKeyAndValue(reqJson, "relCd", "未包含link");


        //判断员工手机号是否重复(员工可根据手机号登录平台)
//        UserDto userDto = new UserDto();
//        userDto.setTel(reqJson.getString("tel"));
//        userDto.setUserFlag("1");
//        userDto.setLevelCd("01"); //员工
//        List<UserDto> users = userV1InnerServiceSMOImpl.queryUsers(userDto);
//        Assert.listIsNull(users, "员工手机号不能重复，请重新输入");

    }

    @Override
    public void doCmd(CmdEvent event, ICmdDataFlowContext context, JSONObject reqJson) throws CmdException, ParseException {


        StoreStaffDto storeStaffDto = new StoreStaffDto();
        storeStaffDto.setStaffId(reqJson.getString("staffId"));
        List<StoreStaffDto> storeStaffDtos = storeUserV1InnerServiceSMOImpl.queryStoreStaffs(storeStaffDto);
        if (!ListUtil.isNull(storeStaffDtos)) {
            updateUser(reqJson);
            //todo 同步人脸至 门禁
            synchronousAccessControl(reqJson.getString("staffId"));
            return;
        }


        //todo 添加用户
        addUser(reqJson);

        //todo 添加商户员工关系
        addStaff(reqJson);

        //todo 添加员工组织关系
        addStaffOrg(reqJson);

        synchronousAccessControl(reqJson.getString("staffId"));

    }


    private void addStaffOrg(JSONObject reqJson) {

        OrgDto orgDto = new OrgDto();
        orgDto.setStoreId(reqJson.getString("propertyId"));
        orgDto.setOrgLevel("1");
        List<OrgDto> orgDtos = orgV1InnerServiceSMOImpl.queryOrgs(orgDto);

        if (ListUtil.isNull(orgDtos)) {
            return;
        }


        //添加组织 员工关系
        OrgStaffRelPo orgStaffRelPo = new OrgStaffRelPo();
        orgStaffRelPo.setOrgId(orgDtos.get(0).getOrgId());
        orgStaffRelPo.setStaffId(reqJson.getString("staffId"));
        orgStaffRelPo.setRelId(GenerateCodeFactory.getGeneratorId("11"));
        orgStaffRelPo.setRelCd(OrgStaffRelDto.REL_CD_ADMIN);
        orgStaffRelPo.setStoreId(reqJson.getString("propertyId"));
        int flag = orgStaffRelV1InnerServiceSMOImpl.saveOrgStaffRel(orgStaffRelPo);
        if (flag < 1) {
            throw new CmdException("注册失败");
        }
    }

    private void addStaff(JSONObject reqJson) {

        //保存 商户和用户的关系
        StoreStaffPo storeUserPo = new StoreStaffPo();
        storeUserPo.setStoreStaffId(GenerateCodeFactory.getGeneratorId("11"));
        storeUserPo.setAdminFlag(StoreStaffDto.ADMIN_FLAG_N);

        storeUserPo.setStoreId(reqJson.getString("propertyId"));
        storeUserPo.setStaffId(reqJson.getString("staffId"));
        storeUserPo.setTel(reqJson.getString("tel"));
        storeUserPo.setStaffName(reqJson.getString("name"));
        int flag = storeUserV1InnerServiceSMOImpl.saveStoreStaff(storeUserPo);
        if (flag < 1) {
            throw new CmdException("注册失败");
        }

    }

    /**
     * Assert.hasKeyAndValue(reqJson, "propertyId", "未包含communityId");
     * Assert.hasKeyAndValue(reqJson, "staffId", "未包含ownerId");
     * Assert.hasKeyAndValue(reqJson, "name", "未包含name");
     * Assert.hasKeyAndValue(reqJson, "tel", "未包含link");
     * Assert.hasKeyAndValue(reqJson, "relCd", "未包含link");
     *
     * @param reqJson
     */
    private void addUser(JSONObject reqJson) {

        String faceUrl = getFaceUrl(reqJson);

        UserPo userPo = new UserPo();
        userPo.setTel(reqJson.getString("tel"));
        userPo.setName(reqJson.getString("name"));
        userPo.setAddress("无");
        userPo.setUserId(reqJson.getString("staffId"));
        userPo.setEmail("无");
        userPo.setLevelCd(UserDto.LEVEL_CD_STAFF);
        userPo.setSex("1");
        userPo.setFaceUrl(faceUrl);
        //设置默认密码
        String staffDefaultPassword = STAFF_DEFAULT_PASSWORD;
        staffDefaultPassword = AuthenticationFactory.passwdMd5(staffDefaultPassword);
        userPo.setPassword(staffDefaultPassword);
        userV1InnerServiceSMOImpl.saveUser(userPo);
    }


    private void updateUser(JSONObject reqJson) {

        String faceUrl = getFaceUrl(reqJson);

        UserPo userPo = new UserPo();
        userPo.setTel(reqJson.getString("tel"));
        userPo.setName(reqJson.getString("name"));
        userPo.setUserId(reqJson.getString("staffId"));
        userPo.setFaceUrl(faceUrl);
        userV1InnerServiceSMOImpl.updateUser(userPo);
    }

    private String getFaceUrl(JSONObject reqJson) {
        if (!reqJson.containsKey("staffPhoto")) {
            return "";
        }

        String staffPhoto = reqJson.getString("staffPhoto");

        if (StringUtil.isEmpty(staffPhoto)) {
            return "";
        }

        FileDto fileDto = new FileDto();
        fileDto.setFileId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_file_id));
        fileDto.setFileName(fileDto.getFileId());
        fileDto.setContext(reqJson.getString("staffPhoto"));
        fileDto.setSuffix("jpeg");
        fileDto.setCommunityId("-1");
        String fileName = fileInnerServiceSMOImpl.saveFile(fileDto);

        return fileName;
    }

    private void synchronousAccessControl(String staffId) {


        //todo 查询人员信息
        UserDto userDto = new UserDto();
        userDto.setUserId(staffId);
        List<UserDto> userDtos = userV1InnerServiceSMOImpl.queryUsers(userDto);
        if (ListUtil.isNull(userDtos)) {
            return;
        }
        userDto = userDtos.get(0);

        //todo 查询业主照片
        String imgUrl = MappingCache.getValue(MappingConstant.FILE_DOMAIN, "IMG_PATH");

        String url = userDto.getFaceUrl();
        if (!url.startsWith("http")) {
            url = imgUrl + userDto.getFaceUrl();
        }


        AccessControlFaceDto accessControlFaceDto = new AccessControlFaceDto();
        accessControlFaceDto.setPersonId(staffId);
        List<AccessControlFaceDto> accessControlFaceDtos = accessControlFaceV1InnerServiceSMOImpl.queryAccessControlFaces(accessControlFaceDto);
        AccessControlFacePo accessControlFacePo = null;
        if (!ListUtil.isNull(accessControlFaceDtos)) {
            for (AccessControlFaceDto tmpAccessControlFaceDto : accessControlFaceDtos) {
                accessControlFacePo = BeanConvertUtil.covertBean(tmpAccessControlFaceDto, AccessControlFacePo.class);
                accessControlFacePo.setState(AccessControlFaceDto.STATE_WAIT);
                accessControlFacePo.setMessage("待下发到门禁");
                accessControlFacePo.setFacePath(url);
                accessControlFacePo.setCardNumber("");
                accessControlFacePo.setIdNumber("");
                accessControlFacePo.setName(userDto.getName());
                accessControlFaceV1InnerServiceSMOImpl.updateAccessControlFace(accessControlFacePo);
            }
            return;
        }

        //todo

        OrgStaffRelDto orgStaffRelDto = new OrgStaffRelDto();
        orgStaffRelDto.setStaffId(staffId);
        List<OrgStaffRelDto> orgStaffRelDtoList = orgStaffRelInnerServiceSMOImpl.queryOrgInfoByStaffIdsNew(orgStaffRelDto);

        if (ListUtil.isNull(orgStaffRelDtoList)) {
            return;
        }

        AccessControlOrgDto accessControlOrgDto = new AccessControlOrgDto();
        accessControlOrgDto.setOrgId(orgStaffRelDtoList.get(0).getOrgId());
        List<AccessControlOrgDto> accessControlOrgDtos = accessControlOrgV1InnerServiceSMOImpl.queryAccessControlOrgs(accessControlOrgDto);
        if (ListUtil.isNull(accessControlOrgDtos)) {
            return;
        }

        for (AccessControlOrgDto tmpAccessControlOrgDto : accessControlOrgDtos) {
            accessControlFacePo = new AccessControlFacePo();
            accessControlFacePo.setFacePath(url);
            accessControlFacePo.setCommunityId(tmpAccessControlOrgDto.getCommunityId());
            accessControlFacePo.setIdNumber("");
            accessControlFacePo.setComeId(tmpAccessControlOrgDto.getAcoId());
            accessControlFacePo.setComeType(AccessControlFaceDto.COME_TYPE_STAFF);
            accessControlFacePo.setEndTime(DateUtil.getLastTime());
            accessControlFacePo.setStartTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_B));
            accessControlFacePo.setCardNumber("");
            accessControlFacePo.setMachineId(tmpAccessControlOrgDto.getMachineId());
            accessControlFacePo.setMessage("待下发到门禁");
            accessControlFacePo.setMfId(GenerateCodeFactory.getGeneratorId("11"));
            accessControlFacePo.setName(userDto.getName());
            accessControlFacePo.setPersonId(userDto.getUserId());
            accessControlFacePo.setPersonType(AccessControlFaceDto.PERSON_TYPE_STAFF);
            accessControlFacePo.setState(AccessControlFaceDto.STATE_WAIT);
            accessControlFacePo.setRoomName("无");
            accessControlFaceV1InnerServiceSMOImpl.saveAccessControlFace(accessControlFacePo);
        }



    }
}
