package com.java110.dto.lamp;

import com.java110.bean.dto.PageDto;

import java.io.Serializable;
import java.util.Date;

public class LampMachineDto extends PageDto implements Serializable {
    public static final String LAMP_TURN_ON_STATE = "1001";
    public static final String LAMP_TURN_OFF_STATE = "1002";

    public static final String STATE_ONLINE = "ONLINE";
    public static final String STATE_OFFLINE = "OFFLINE";

    private String machineId;
    private String machineName;
    private String machineCode;
    private String locationName;
    private String factoryId;
    private String communityId;
    private Date heartbeatTime;
    private String state;
    private Date createTime;
    private String statusCd = "0";

    private String factoryName;
    private String stateName;

    private String logAction;
    private String userId;
    private String userName;
    private Date operationTime;
    private String isOnlineState;
    private String isOnlineStateName;

    public String getMachineId() {
        return machineId;
    }

    public void setMachineId(String machineId) {
        this.machineId = machineId;
    }

    public String getMachineName() {
        return machineName;
    }

    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    public String getMachineCode() {
        return machineCode;
    }

    public void setMachineCode(String machineCode) {
        this.machineCode = machineCode;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getFactoryId() {
        return factoryId;
    }

    public void setFactoryId(String factoryId) {
        this.factoryId = factoryId;
    }

    public String getCommunityId() {
        return communityId;
    }

    public void setCommunityId(String communityId) {
        this.communityId = communityId;
    }

    public Date getHeartbeatTime() {
        return heartbeatTime;
    }

    public void setHeartbeatTime(Date heartbeatTime) {
        this.heartbeatTime = heartbeatTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getFactoryName() {
        return factoryName;
    }

    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getLogAction() {
        return logAction;
    }

    public void setLogAction(String logAction) {
        this.logAction = logAction;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getOperationTime() {
        return operationTime;
    }

    public void setOperationTime(Date operationTime) {
        this.operationTime = operationTime;
    }

    public String getIsOnlineState() {
        return isOnlineState;
    }

    public void setIsOnlineState(String isOnlineState) {
        this.isOnlineState = isOnlineState;
    }

    public String getIsOnlineStateName() {
        return isOnlineStateName;
    }

    public void setIsOnlineStateName(String isOnlineStateName) {
        this.isOnlineStateName = isOnlineStateName;
    }
}
